package nino.macut.afl.ui.locations;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import nino.macut.afl.R;
import nino.macut.afl.classes.Location;

public class LocationsFragment extends Fragment {

    public static LocationsFragment newInstance() {
        return new LocationsFragment();
    }

    private ArrayList<Location> locationList = new ArrayList<>();
    private ListView listOfLocations;

    String FILENAME = "locations";

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_locations, container, false);

        verifyPermissions();
        loadLocations();
        initializeList(root);
        initializeAddButton(root);

        return root;
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void initializeList(View root){
        listOfLocations = root.findViewById(R.id.list_locations);
        listOfLocations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), "Tu dodaj svasta", Toast.LENGTH_SHORT).show();
                locationList.remove(position);
                saveLocations();
                loadLocations();
                loadList();
            }
        });
        loadList();
    }

    private void initializeAddButton(View root){
        Button buttonAddLocation = root.findViewById(R.id.button_add_location);
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        buttonAddLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.nav_add_location);
            }
        });
    }

    private void loadList(){
        Log.d("test", "loadam listu lokacija");
        LocationsAdapter locationsAdapter = new LocationsAdapter(getActivity(), locationList);
        listOfLocations.setAdapter(locationsAdapter);
    }

    private void saveLocations(){
        try {
            FileOutputStream fos = getActivity().openFileOutput(FILENAME, getActivity().MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(locationList);
            os.close();
            fos.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void loadLocations(){
        try {
            FileInputStream fis = getActivity().openFileInput(FILENAME);
            ObjectInputStream is = new ObjectInputStream(fis);
            locationList = (ArrayList<Location>) is.readObject();
            is.close();
            fis.close();
        }catch (IOException ioe){
            ioe.printStackTrace();
            return;
        }catch (ClassNotFoundException c){
            c.printStackTrace();
            return;
        }
    }

    private Bitmap loadImageFromStorage(String fileName)
    {
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Locations", Context.MODE_PRIVATE);
        try {
            File f=new File(directory, fileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public class LocationsAdapter extends ArrayAdapter<Location> {
        public LocationsAdapter(Context context, ArrayList<Location> locationArrayList){
            super(context, R.layout.listitem_location, locationArrayList);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            Location location = getItem(position);

            if (convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_location,parent,false);
            }

            ImageView image = convertView.findViewById(R.id.image_location_preview_listitem);
            TextView name = convertView.findViewById(R.id.text_location_name_listitem);
            TextView type = convertView.findViewById(R.id.text_location_type_listitem);

            Log.d("test", location.image);
            if (!location.image.equals("nofile")){
                Log.d("test", "uso u if");
                Bitmap bitmap = loadImageFromStorage(location.image);
                image.setImageBitmap(bitmap);
            }
            else if (location.image.equals("nofile")){
                Log.d("test", "uso u else if");
                image.setImageResource(R.drawable.ic_location_no_pic);
            }

            name.setText(location.name + "");
            type.setText(location.type + "");
            return convertView;
        }
    }
}