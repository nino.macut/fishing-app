package nino.macut.afl.ui.fishermen;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import nino.macut.afl.R;
import nino.macut.afl.classes.Fisherman;

public class FishermenFragment extends Fragment {

    public static FishermenFragment newInstance() {
        return new FishermenFragment();
    }

    private ArrayList<Fisherman> fishermanList = new ArrayList<>();
    private ListView listOfFishermen;

    String FILENAME = "fishermen";

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_fishermen, container, false);

        verifyPermissions();
        loadFishermen();
        initializeList(root);
        initializeAddButton(root);

        return root;
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void initializeList(View root){
        listOfFishermen = root.findViewById(R.id.list_fishermen);
        listOfFishermen.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), "Tu dodaj svasta", Toast.LENGTH_SHORT).show();
                fishermanList.remove(position);
                saveFishermen();
                loadFishermen();
                loadList();
            }
        });
        loadList();
    }

    private void initializeAddButton(View root){
        Button buttonAddFisherman = root.findViewById(R.id.button_add_fisherman);
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        buttonAddFisherman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.nav_add_fisherman);
            }
        });
    }

    private void loadList(){
        Log.d("test", "loadam listu ribica");
        FishermenAdapter fishermenAdapter = new FishermenAdapter(getActivity(), fishermanList);
        listOfFishermen.setAdapter(fishermenAdapter);
    }

    private void deleteFishermen(){
        File f = new File(FILENAME);
        if (f.exists()){
            f.delete();
        }

    }

    private void saveFishermen(){
        try {
            FileOutputStream fos = getActivity().openFileOutput(FILENAME, getActivity().MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(fishermanList);
            os.close();
            fos.close();
        }catch (Exception e){

        }
    }

    private void loadFishermen(){
        try {
            FileInputStream fis = getActivity().openFileInput(FILENAME);
            ObjectInputStream is = new ObjectInputStream(fis);
            fishermanList = (ArrayList<Fisherman>) is.readObject();
            is.close();
            fis.close();
        }catch (IOException ioe){
            ioe.printStackTrace();
            return;
        }catch (ClassNotFoundException c){
            c.printStackTrace();
            return;
        }
    }

    private Bitmap loadImageFromStorage(String fileName)
    {
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Fishermen", Context.MODE_PRIVATE);
        try {
            File f=new File(directory, fileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public class FishermenAdapter extends ArrayAdapter<Fisherman> {
        public FishermenAdapter(Context context, ArrayList<Fisherman> fishermenArrayList){
            super(context, R.layout.listitem_fisherman, fishermenArrayList);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            Fisherman fisherman = getItem(position);

            if (convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_fisherman,parent,false);
            }

            ImageView image = convertView.findViewById(R.id.image_fisherman_portrait_listitem);
            TextView fullName = convertView.findViewById(R.id.text_fisherman_full_name_listitem);
            TextView dateOfBirth = convertView.findViewById(R.id.text_fisherman_date_of_birth_listitem);

            Log.d("test", fisherman.image);
            if (!fisherman.image.equals("nofile")){
                Log.d("test", "uso u if");
                Bitmap bitmap = loadImageFromStorage(fisherman.image);
                image.setImageBitmap(bitmap);
            }
            else if (fisherman.image.equals("nofile")){
                Log.d("test", "uso u else if");
                image.setImageResource(R.drawable.ic_fisherman_no_pic);
            }


            fullName.setText(fisherman.firstName + " " + fisherman.lastName);
            dateOfBirth.setText(fisherman.dateOfBirth + "");
            return convertView;
        }
    }
}