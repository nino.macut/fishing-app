package nino.macut.afl.classes;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.widget.ImageView;

import java.io.Serializable;
import java.util.Date;

public class Fisherman implements Serializable {
    public String image;
    public String dateOfBirth;
    public String firstName;
    public String lastName;

    public Fisherman(String image, String dateOfBirth, String firstName, String lastName){
        this.image = image;
        this.dateOfBirth = dateOfBirth;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
